"""myprofile URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.urls import re_path, include
from django.conf.urls import url, include
from story4.views import homepage as pageone
from story4.views import homepage2 as pagetwo
from story4.views import regist as registform
from story4.views import tambahJadwal as pagefour
from story4.views import jadwal as pagefive
from story4.views import jadwal 
from story4.views import deleteJadwal as hapusJadwal

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('story4.urls')),
    re_path(r'story3 - page1', pageone, name="pageone"),
    re_path(r'story3 - page2', pagetwo, name="pagetwo"),
    re_path(r'regist', registform, name="registform"),
    re_path(r'bikinJadwal', pagefour, name="tambahJadwal"),
    re_path(r'jadwal', pagefive, name="jadwal"),
    re_path(r'listJadwal', jadwal, name="listJadwal"),
    re_path(r'deleteJadwal', hapusJadwal, name="deleteJadwal")
]
