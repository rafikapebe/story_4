from django.db import models

class Schedule(models.Model):
	kegiatan = models.CharField(max_length=150)
	hari = models.CharField(max_length=20)
	tanggal = models.DateField()
	waktu = models.TimeField()
	tempat = models.CharField(max_length=100)
	kategori = models.CharField(max_length=100)