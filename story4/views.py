from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Schedule
from .forms import Schedule_Form


def homepage(request):
	return render(request, "story3 - page1.html")

def homepage2(request):
	return render(request, "story3 - page2.html")

def regist(request):
	return render(request, "regist.html")

def jadwal(request):
	response = {}
	lstJadwal = Schedule.objects.all()
	response = {
		"lstJadwal" : lstJadwal
	}
	print (response)
	print (lstJadwal)
	return render(request, "jadwal.html", response)


def tambahJadwal(request):
	form = Schedule_Form(request.POST or None)
	response = {}
	if(request.method == "POST"):
		if(form.is_valid()):
			kegiatan = request.POST.get("kegiatan")
			hari = request.POST.get("hari")
			tanggal = request.POST.get("tanggal")
			waktu = request.POST.get("waktu")
			tempat = request.POST.get("tempat")
			kategori = request.POST.get("kategori")

			Schedule.objects.create(
				kegiatan=kegiatan,
				hari = hari,
				tanggal = tanggal,
				waktu = waktu,
				tempat = tempat,
				kategori = kategori,
			)

			return redirect('/listJadwal')
		else:
			return render(request, 'bikinJadwal.html', response)
	else:
		response['form'] = form
		return render(request, 'bikinJadwal.html', response)

def deleteJadwal(request):
	Schedule.objects.all().delete()
	response = {}
	return render(request, 'jadwal.html', response)