from django import forms
from .models import Schedule

class Schedule_Form(forms.Form):
	kegiatan = forms.CharField(label="Activity")
	hari = forms.CharField(label="Day")
	tanggal = forms.DateField(widget=forms.DateInput(attrs={'type':'date'}), label="Date")
	waktu = forms.TimeField(widget=forms.TimeInput(attrs={'type':'time'}), label="Time")
	tempat = forms.CharField(label="Tempat")
	kategori = forms.CharField(label="Category")
