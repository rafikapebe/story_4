from django.urls import path
from . import views
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
	path('', views.homepage, name="story3 - page1"),
	path('', views.homepage2, name="story3 - page2"),
	path('', views.regist, name="regist"),
	path('', views.tambahJadwal, name="bikinJadwal"),
    path('', views.jadwal, name="jadwal"),
    path('', views.deleteJadwal, name="jadwal.html")
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)